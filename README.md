# Spacex web app

This is a web application built with `React js` that bootstraped with `create-react-app`

## Technologies

- [x] Using `react-lazyload` for lazy loading the components and rendering the components that are in view.
- [x] Using `react-jss` for styling the components animating the DOM.
- [x] Using `Axios` for Network requests.
- [x] Using `redux` for global state management.
- [x] Using `redux-thunk` for handling async actions.

## App structure

```
-- src/
---- ├── App.js
---- ├── Assets
---- │   ├── css
---- │   │   └── font.css
---- │   ├── font
---- │   │   ├── Atures300PersonalUseOnly.ttf
---- │   │   ├── Atures500PersonalUseOnly.ttf
---- │   │   ├── Atures700PersonalUseOnly.ttf
---- │   │   └── Atures900PersonalUseOnly.ttf
---- │   └── images
---- │       └── mars.png
---- ├── Components
---- │   ├── Card
---- │   │   ├── Card.jsx
---- │   │   └── Card.styles.js
---- │   ├── Header
---- │   │   ├── Header.jsx
---- │   │   └── Header.styles.js
---- │   ├── Hooks
---- │   │   └── useDebounce
---- │   │       └── index.js
---- │   ├── Input
---- │   │   ├── Input.jsx
---- │   │   └── Input.styles.js
---- │   ├── Layout
---- │   │   ├── Layout.jsx
---- │   │   └── Layout.styles.js
---- │   ├── LinkItem
---- │   │   ├── LinkItem.jsx
---- │   │   └── LinkItem.styles.js
---- │   ├── Partials
---- │   │   ├── AllMissions
---- │   │   │   ├── AllMissions.jsx
---- │   │   │   └── AllMissions.styles.js
---- │   │   ├── HomeMission
---- │   │   │   ├── HomeMission.jsx
---- │   │   │   └── HomeMission.styles.js
---- │   │   ├── PastLaunches
---- │   │   │   └── PastLaunches.jsx
---- │   │   ├── SearchLaunches
---- │   │   │   ├── SearchLaunches.jsx
---- │   │   │   └── SearchLaunches.styles.js
---- │   │   └── UpcomingLaunches
---- │   │       └── UpcomingLaunches.jsx
---- │   └── Tab
---- │       ├── Tab.jsx
---- │       └── Tab.styles.js
---- ├── Constants
---- │   ├── api.js
---- │   ├── constants.js
---- │   └── helpers.js
---- ├── index.css
---- ├── index.js
---- ├── Pages
---- │   ├── Home
---- │   │   ├── Home.jsx
---- │   │   └── Home.styles.js
---- │   ├── Launches
---- │   │   ├── Launches.jsx
---- │   │   └── Launches.styles.js
---- │   ├── Loading
---- │   │   ├── Loading.jsx
---- │   │   └── Loading.styles.js
---- │   ├── Missions
---- │   │   ├── Missions.jsx
---- │   │   └── Missions.styles.js
---- │   └── Notfound
---- │       ├── Notfound.jsx
---- │       └── Notfound.styles.js
---- ├── Redux
---- │   ├── actions
---- │   │   └── fetchActions.js
---- │   ├── reducers
---- │   │   ├── index.js
---- │   │   ├── launches.js
---- │   │   └── missions.js
---- │   └── types.js
---- ├── serviceWorker.js
---- ├── setupTests.js
---- ├── store.js
---- ├── .editorconfig
---- ├── Theme
----     └── index.js

-- public/
---- ├── favicon.ico
---- ├── index.html
---- ├── logo192.png
---- ├── logo512.png
---- ├── manifest.json
---- ├── robots.txt
---- └── tree.txt
```

The project includes 6 important directories :

- Assets : All assets including fonts,css,images.
- Components : All react components including CoreComponent (like input , card , tab , ...) , Partial (the Component that used directly in pages) and hooks.
- Constants : Constants like constats variables and useful functions.
- Pages : Pages of application
- Redux : All redux structure including actions , reducers and types.
- Theme : All theme variables are here.

## How to initiate the project :

to start the project simply `clone` project and after that you need to install the depencencies. to achive that simply run the command below

```bash
npm install
```

or run

```bash
yarn
```

## How to start the project

to start development server need to run the start script like so :

```bash
npm run start
```

or

```bash
yarn start
```

and look for `http://localhost:3000` in your browser.

## How to build the project

to build the project you need to run build script like so :

```bash
npm run build
```

or

```bash
yarn build
```

and then you can serve it by installing `serve` package and serve the `build` directory.

## How to contribute

for contributing on project you can clone the project and start to develop it.

**Please read before contributing**

- Assets : Please put all your assets including (Images , css files, fonts) on this directory

- Components : Please put all your core components on `Components` directory.
  Use `*.styles.js` for naming `jss` files.
  Use `*.jsx` for naming `react` view Components files.

- Constants : Please put all your useful functions and constants on this folder.
- Pages : Please put all your page component on this directory.

- Redux : Please put all your action files and reducers and types on this directory.

- Theme : Please put all your diffrent theme variables on this directory.
