export default {
  superColor: "#ff7f00",
  textColor: "#fff",
  headingColor: "#eee",
  borderStyle: "1px solid #eee",
};
