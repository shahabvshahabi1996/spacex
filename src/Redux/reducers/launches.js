import { launchTypes } from "../types";

const initialState = {
  past: { data: [], isError: false, loading: false },
  upcoming: { data: [], isError: false, loading: false },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case launchTypes.FETCH_PAST_LAUNCHES_LOADING: {
      return {
        ...state,
        past: {
          ...state.past,
          loading: true,
        },
      };
    }

    case launchTypes.FETCH_PAST_LAUNCHES_SUCCESS: {
      return {
        ...state,
        past: {
          loading: false,
          isError: false,
          data: action.payload,
        },
      };
    }

    case launchTypes.FETCH_PAST_LAUNCHES_FAILED: {
      return {
        ...state,
        past: {
          loading: false,
          isError: true,
          data: [],
        },
      };
    }

    case launchTypes.FETCH_UPCOMING_LAUNCHES_LOADING: {
      return {
        ...state,
        upcoming: {
          ...state.past,
          loading: true,
        },
      };
    }

    case launchTypes.FETCH_UPCOMING_LAUNCHES_SUCCESS: {
      return {
        ...state,
        upcoming: {
          loading: false,
          isError: false,
          data: action.payload,
        },
      };
    }

    case launchTypes.FETCH_UPCOMING_LAUNCHES_FAILED: {
      return {
        ...state,
        upcoming: {
          loading: false,
          isError: true,
          data: [],
        },
      };
    }

    default: {
      return state;
    }
  }
}
