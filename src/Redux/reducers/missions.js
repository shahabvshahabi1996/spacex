import { missionTypes } from "../types";

const initialState = {
  home: {
    data: [],
    isError: false,
    loading: false,
  },
  all: {
    data: [],
    isError: false,
    loading: false,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case missionTypes.FETCH_HOME_MISSIONS_LOADING: {
      return {
        ...state,
        home: {
          ...state.home,
          loading: true,
        },
      };
    }
    case missionTypes.FETCH_HOME_MISSIONS_SUCCESS: {
      return {
        ...state,
        home: {
          data: action.payload,
          isError: false,
          loading: false,
        },
      };
    }

    case missionTypes.FETCH_HOME_MISSIONS_FAILED: {
      return {
        ...state,
        home: {
          data: [],
          isError: true,
          loading: false,
        },
      };
    }

    case missionTypes.FETCH_ALL_MISSIONS_LOADING: {
      return {
        ...state,
        all: {
          ...state.home,
          loading: true,
        },
      };
    }
    case missionTypes.FETCH_ALL_MISSIONS_SUCCESS: {
      return {
        ...state,
        all: {
          data: action.payload,
          isError: false,
          loading: false,
        },
      };
    }

    case missionTypes.FETCH_ALL_MISSIONS_FAILED: {
      return {
        ...state,
        all: {
          data: [],
          isError: false,
          loading: false,
        },
      };
    }

    default: {
      return state;
    }
  }
}
