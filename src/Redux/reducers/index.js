import { combineReducers } from "redux";
import MissionsReducer from "./missions";
import LaunchesReducer from "./launches";

export default combineReducers({
  missions: MissionsReducer,
  launches: LaunchesReducer,
});
