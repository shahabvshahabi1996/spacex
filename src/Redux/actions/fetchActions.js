import API from "../../Constants/api";
import {
  GET_MISSIONS,
  GET_PAST_LAUNCHES,
  GET_UPCOMING_LAUNCHES,
} from "../../Constants/constants";
import { queryMaker } from "../../Constants/helpers";
import { missionTypes, launchTypes } from "../types";

const api = new API();

export const fetchHomeMissions = () => {
  return (dispatch) => {
    dispatch({ type: missionTypes.FETCH_HOME_MISSIONS_LOADING });
    let data = {
      limit: 3,
    };

    let query = queryMaker(data);

    api
      ._GET(GET_MISSIONS(query))
      .then((res) =>
        dispatch({
          type: missionTypes.FETCH_HOME_MISSIONS_SUCCESS,
          payload: res.data.map((item) => {
            return {
              mission_name: item.mission_name,
              mission_id: item.mission_id,
              description: item.description
                ? item.description
                : "There is no data to show",
              wikipedia: item.wikipedia,
            };
          }),
        })
      )
      .catch(() => dispatch({ type: missionTypes.FETCH_HOME_MISSIONS_FAILED }));
  };
};

export const fetchAllMissions = () => {
  return (dispatch) => {
    dispatch({ type: missionTypes.FETCH_ALL_MISSIONS_LOADING });
    api
      ._GET(GET_MISSIONS())
      .then((res) =>
        dispatch({
          type: missionTypes.FETCH_ALL_MISSIONS_SUCCESS,
          payload: res.data.map((item) => {
            return {
              mission_name: item.mission_name,
              mission_id: item.mission_id,
              description: item.description
                ? item.description
                : "There is no data to show",
              wikipedia: item.wikipedia,
            };
          }),
        })
      )
      .catch(() => dispatch({ type: missionTypes.FETCH_ALL_MISSIONS_FAILED }));
  };
};

export const fetchPastLaunches = () => {
  return (dispatch) => {
    dispatch({ type: launchTypes.FETCH_PAST_LAUNCHES_LOADING });
    api
      ._GET(GET_PAST_LAUNCHES())
      .then((res) =>
        dispatch({
          type: launchTypes.FETCH_PAST_LAUNCHES_SUCCESS,
          payload: res.data.reverse().map((item) => {
            return {
              mission_name: item.mission_name,
              flight_number: item.flight_number,
              details: item.details ? item.details : "There is no data to show",
              launch_date_utc: item.launch_date_utc,
              launch_site: item.launch_site,
            };
          }),
        })
      )
      .catch((e) =>
        dispatch({ type: launchTypes.FETCH_PAST_LAUNCHES_FAILED, payload: e })
      );
  };
};

export const fetchUpcomingLaunches = () => {
  return (dispatch) => {
    dispatch({ type: launchTypes.FETCH_UPCOMING_LAUNCHES_LOADING });
    api
      ._GET(GET_UPCOMING_LAUNCHES())
      .then((res) =>
        dispatch({
          type: launchTypes.FETCH_UPCOMING_LAUNCHES_SUCCESS,
          payload: res.data.map((item) => {
            return {
              mission_name: item.mission_name,
              flight_number: item.flight_number,
              details: item.details ? item.details : "There is no data to show",
              launch_date_utc: item.launch_date_utc,
              launch_site: item.launch_site,
            };
          }),
        })
      )
      .catch((e) =>
        dispatch({
          type: launchTypes.FETCH_UPCOMING_LAUNCHES_FAILED,
          payload: e,
        })
      );
  };
};
