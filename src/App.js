import React, { Suspense } from "react";
import { ThemeProvider } from "react-jss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Loading from "./Pages/Loading/Loading";
import theme from "./Theme";

const lazyHome = React.lazy(() => import("./Pages/Home/Home"));
const lazyLaunches = React.lazy(() => import("./Pages/Launches/Launches"));
const lazyMissions = React.lazy(() => import("./Pages/Missions/Missions"));
const lazyNotFound = React.lazy(() => import("./Pages/Notfound/Notfound"));

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact={true} path="/" component={lazyHome} />
            <Route path="/launches" component={lazyLaunches} />
            <Route path="/missions" component={lazyMissions} />
            <Route path="*" component={lazyNotFound} />
          </Switch>
        </Suspense>
      </Router>
    </ThemeProvider>
  );
};

export default App;
