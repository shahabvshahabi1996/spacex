import React from "react";
import Styles from "./Missions.styles";
import Layout from "../../Components/Layout/Layout";
import AllMissions from "../../Components/Partials/AllMissions/AllMissions";
const Missions = () => {
  const classes = Styles();

  return (
    <Layout>
      <div className={classes.root}>
        <h1>Missions</h1>
        <div className={classes.contentWrapper}>
          <AllMissions />
        </div>
      </div>
    </Layout>
  );
};

export default Missions;
