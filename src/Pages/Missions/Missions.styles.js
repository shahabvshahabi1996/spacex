import { createUseStyles } from "react-jss";

export default createUseStyles(({ textColor, borderStyle }) => ({
  contentWrapper: {
    borderTop: borderStyle,
    margin: "30px 0px",
    paddingTop: 20,
    color: textColor,
  },
}));
