import React from "react";
import Styles from "./Loading.styles";

const Loading = () => {
  const classes = Styles();
  return (
    <div className={classes.root}>
      <h1>LOADING ...</h1>
    </div>
  );
};

export default Loading;
