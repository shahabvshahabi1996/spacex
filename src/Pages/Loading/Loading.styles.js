import { createUseStyles } from "react-jss";

export default createUseStyles(({ textColor }) => ({
  root: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: textColor,
    backgroundColor: "#000",
  },
}));
