import { createUseStyles } from "react-jss";

export default createUseStyles(({ borderStyle, textColor }) => ({
  toolbar: {
    borderTop: borderStyle,
    borderBottom: borderStyle,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "30px 0px",
    flexWrap: "wrap",
    zIndex: 1,
  },
  tabBar: {
    display: "flex",
    alignItems: "center",
  },

  inputContainer: {
    display: "flex",
    alignItems: "center",
    borderLeft: borderStyle,
    padding: 10,
    width: 50,
    "@media screen and (max-width:769px)": {
      width: "100%",
      border: "none",
    },
  },

  searchText: {
    display: "none",
    "@media screen and (max-width:769px)": {
      display: "block",
      marginLeft: 5,
    },
  },

  contentWrapper: {
    color: textColor,
  },
}));
