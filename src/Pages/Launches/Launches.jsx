import React, { useState } from "react";
import Styles from "./Launches.styles";
import Layout from "../../Components/Layout/Layout";
import PastLaunches from "../../Components/Partials/PastLaunches/PastLaunches";
import Tab from "../../Components/Tab/Tab";
const UpcomingLaunches = React.lazy(() =>
  import("../../Components/Partials/UpcomingLaunches/UpcomingLaunches")
);
const SearchLaunches = React.lazy(() =>
  import("../../Components/Partials/SearchLaunches/SearchLaunches")
);

const Launches = () => {
  const classes = Styles();
  const [activeTab, setActiveTab] = useState("past");

  const renderActiveTab = () => {
    switch (activeTab) {
      case "past": {
        return <PastLaunches />;
      }

      case "upcoming": {
        return <UpcomingLaunches />;
      }

      case "search": {
        return <SearchLaunches />;
      }

      default: {
        return <></>;
      }
    }
  };

  return (
    <Layout>
      <div className={classes.root}>
        <h1>Launches</h1>
        <div className={classes.toolbar}>
          <div className={classes.tabBar}>
            <Tab
              onClick={() => setActiveTab("past")}
              isActive={activeTab === "past"}
            >
              Past Launches
            </Tab>
            <Tab
              onClick={() => setActiveTab("upcoming")}
              isActive={activeTab === "upcoming"}
            >
              Upcoming Launches
            </Tab>
          </div>
          <div className={classes.inputContainer}>
            <Tab
              onClick={() => setActiveTab("search")}
              isActive={activeTab === "search"}
            >
              <i className={"fa fa-search fa-2x"}></i>{" "}
              <span className={classes.searchText}>Search Launches</span>
            </Tab>
          </div>
        </div>
      </div>
      <div className={classes.contentWrapper}>{renderActiveTab()}</div>
    </Layout>
  );
};

export default Launches;
