import { createUseStyles } from "react-jss";

export default createUseStyles(({ textColor, superColor }) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100vw",
    height: "100vh",
    flexDirection: "column",
    backgroundColor: "#000",
    position: "relative",
  },
  content: {
    position: "relative",
    zIndex: 1,
    textAlign: "center",
    "& > h1": {
      fontSize: 100,
      marginBottom: 15,
    },
    "& > a": {
      textDecoration: "none",
      color: textColor,
      fontSize: 30,
      fontWeight: "bold",
      transition: "color 0.5s",
      "&:hover": {
        color: superColor,
      },
    },
  },
  marsImage: {
    position: "absolute",
    zIndex: 0,
    right: 200,
    top: 200,
    transform: "scale(2)",
  },
}));
