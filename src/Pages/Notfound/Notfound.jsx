import React from "react";
import Styles from "./Notfound.styles";
import marsImage from "../../Assets/images/mars.png";
import { Link } from "react-router-dom";

const NotFound = () => {
  const classes = Styles();
  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <h1>404</h1>
        <h2>PAGE NOT FOUND!</h2>
        <Link to="/">GO HOME</Link>
      </div>
      <img className={classes.marsImage} src={marsImage} />
    </div>
  );
};

export default NotFound;
