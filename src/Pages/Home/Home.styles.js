import { createUseStyles } from "react-jss";

export default createUseStyles({
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    height: "500px",
    marginBottom: 10,
    "@media screen and (max-width : 720px)": {
      height: "auto",
    },
  },
  contentText: {
    padding: 10,
    zIndex: 1,
    opacity: 0,
    top: 0,
    width: "70%",
    animation: "$fading",
    animationDuration: "1.5s",
    animationDelay: "0.8s",
    animationFillMode: "forwards",
    position: "relative",
    "@media screen and (max-width : 720px)": {
      width: "100%",
      padding: 5,
    },
  },
  contentImage: {
    position: "absolute",
    zIndex: 0,
    animationName: "$marsAnimation",
    animationDuration: "2s",
    animationFillMode: "forwards",
  },
  missionWrapper: {
    position: "relative",
    zIndex: 1,
    opacity: 0,
    top: 0,
    animation: "$fading",
    animationDuration: "1.5s",
    animationDelay: "1.5s",
    animationFillMode: "forwards",
  },

  "@keyframes marsAnimation": {
    from: {
      top: 0,
      right: 0,
      opacity: 0,
      transform: "rotate(0) scale(1)",
    },
    to: {
      top: -60,
      right: -60,
      opacity: 0.8,
      transform: "rotate(30deg) scale(2)",
    },
  },

  "@keyframes fading": {
    from: {
      opacity: 0,
      top: "10px",
    },

    to: {
      top: 0,
      opacity: 1,
    },
  },
});
