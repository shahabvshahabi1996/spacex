import React from "react";
import Layout from "../../Components/Layout/Layout";
import Styles from "./Home.styles";
import MarsImage from "../../Assets/images/mars.png";
import HomeMission from "../../Components/Partials/HomeMission/HomeMission";

const Home = () => {
  const classes = Styles();

  return (
    <Layout>
      <div>
        <div className={classes.content}>
          <div className={classes.contentText}>
            <h1>MISSION TO MARS</h1>
            <h3>We want to land on mars at any cost</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <img className={classes.contentImage} src={MarsImage} />
        </div>
        <div className={classes.missionWrapper}>
          <h1>SOME OF OUR MiSSION</h1>
          <HomeMission />
        </div>
      </div>
    </Layout>
  );
};

export default Home;
