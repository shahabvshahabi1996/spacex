import axios from "axios";
import { BASE_URL } from "./constants";

const instance = axios.create({
  baseURL: BASE_URL,
});

class API {
  _GET(url) {
    return instance.get(url);
  }
}

export default API;
