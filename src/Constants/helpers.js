export const queryMaker = (obj) => {
  let query = "?";

  Object.keys(obj).forEach((item, index) => {
    if (index === Object.keys(obj).length - 1) {
      query += `${item}=${obj[item]}`;
    } else {
      query += `${item}=${obj[item]}&`;
    }
  });

  return query;
};
