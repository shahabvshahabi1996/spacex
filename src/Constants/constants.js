export const BASE_URL = "https://api.spacexdata.com/v3/";
// mission queries
export const GET_MISSIONS = (queries = "") => `missions${queries}`;
// launched queries
export const GET_PAST_LAUNCHES = () => `launches/past`;
export const GET_UPCOMING_LAUNCHES = () => `launches/upcoming`;
export const SEARCH_LAUNCHES = (queries = "") => `launches${queries}`;
