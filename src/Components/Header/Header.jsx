import React from "react";
import Styles from "./Header.styles";
import LinkItem from "../LinkItem/LinkItem";

const Header = () => {
  const classes = Styles();
  return (
    <div className={classes.root}>
      <div className={classes.brandContainer}>
        <LinkItem path="/">SpaceX</LinkItem>
      </div>
      <ul className={classes.menuContainer}>
        <li>
          <LinkItem path="/launches">Launches</LinkItem>
        </li>
        <li>
          <LinkItem path="/missions">Missions</LinkItem>
        </li>
      </ul>
    </div>
  );
};

export default React.memo(Header);
