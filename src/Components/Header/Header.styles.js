import { createUseStyles } from "react-jss";

export default createUseStyles(({ textColor, borderStyle, superColor }) => ({
  root: {
    color: textColor,
    display: "flex",
    alignItems: "center",
    marginBottom: 30,
    padding: "15px 0px",
    justifyContent: "space-between",
    position: "relative",
    flexWrap: "wrap",
    zIndex: 2,
    borderBottom: borderStyle,
  },
  brandContainer: {
    fontSize: 50,
    flex: 1,
    "& > a": {
      textDecoration: "none",
      fontWeight: "bold",
      color: textColor,
      transition: "color 0.5s",
      "&:hover": {
        color: superColor,
      },
    },
  },
  menuContainer: {
    flex: 1,
    display: "flex",
    listStyle: "none",
    flexShrink: 1,
    justifyContent: "flex-end",
    "& > li > a": {
      fontSize: 20,
      textDecoration: "none",
      color: textColor,
      marginLeft: 50,
      transition: "color 0.5s",
      "&:hover": {
        color: superColor,
      },
    },

    "@media screen and (max-width:720px)": {
      justifyContent: "flex-start",
      marginTop: 20,
      "& > li > a": {
        marginLeft: 0,
        marginRight: 50,
      },
    },
  },
}));
