import React from "react";
import Styles, { launchCardStyles, missionCardStyles } from "./Card.styles";
import PropTypes from "prop-types";

export const LaunchCard = ({
  title,
  description,
  date,
  index,
  length,
  siteName,
}) => {
  const classes = launchCardStyles(index, length);
  const customFormat = new Date(date);
  return (
    <div className={classes.launchCard}>
      <h2>SITE NAME : {siteName ? siteName : "NO SITE NAME FOUND"}</h2>
      <h3>
        MISSION NAME : {title} - DATE :{" "}
        <span>{customFormat.toDateString()}</span>
      </h3>
      <p>DETAILES : {description}</p>
    </div>
  );
};

LaunchCard.defaultProps = {
  description: "There is no data to show",
  siteName: "NO SITE NAME FOUND",
};

LaunchCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  date: PropTypes.string,
  index: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired,
  siteName: PropTypes.string,
};

export const MissionCard = ({ title, description, link, index, length }) => {
  const classes = missionCardStyles(index, length);

  return (
    <a target={"__blank"} href={link} className={classes.missionCard}>
      <h2>MISSION NAME : {title}</h2>
      <p>DETAILS : {description}</p>
    </a>
  );
};

MissionCard.defaultProps = {
  description: "There is no data to show",
};

MissionCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string,
  index: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired,
};

const Card = ({ title, description, link }) => {
  const classes = Styles();

  return (
    <a target={"__blank"} href={link} className={classes.root}>
      <h2>MISSION NAME : {title}</h2>
      <p>DETAILS : {description}</p>
    </a>
  );
};

Card.defaultProps = {
  description: "There is no data to show",
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string,
};

export default Card;
