import { createUseStyles } from "react-jss";

export const launchCardStyles = createUseStyles(
  ({ borderStyle, superColor }) => ({
    launchCard: {
      padding: 15,
      margin: "10px",
      border: borderStyle,
      borderRadius: 8,
      maxWidth: "100%",
      position: "relative",
      zIndex: 1,
      animation: "$fading",
      animationDuration: "0.8s",
      animationFillMode: "forwards",
      animationDelay: ({ index, length }) => {
        return `${index / length}s`;
      },
      "@media screen and (max-width:720px)": {
        flexBasis: "100%",
      },
      "& > h2": {
        color: superColor,
      },
    },

    "@keyframes fading": {
      from: {
        top: 30,
        opacity: 0,
      },

      to: {
        top: 0,
        opacity: 1,
      },
    },
  })
);

export const missionCardStyles = createUseStyles(
  ({ borderStyle, superColor }) => ({
    missionCard: {
      textDecoration: "none",
      display: "block",
      padding: 15,
      margin: "10px",
      border: borderStyle,
      borderRadius: 8,
      maxWidth: "100%",
      position: "relative",
      zIndex: 1,
      animation: "$fading",
      animationDuration: "0.8s",
      animationFillMode: "forwards",
      animationDelay: ({ index, length }) => {
        return `${index / length}s`;
      },
      "@media screen and (max-width:720px)": {
        flexBasis: "100%",
      },
      "& > h2": {
        color: superColor,
      },
    },

    "@keyframes fading": {
      from: {
        top: 30,
        opacity: 0,
      },

      to: {
        top: 0,
        opacity: 1,
      },
    },
  })
);

export default createUseStyles(({ borderStyle }) => ({
  root: {
    textDecoration: "none",
    flex: 1,
    flexBasis: "33%",
    padding: 15,
    margin: "10px",
    border: borderStyle,
    borderRadius: 8,
    maxWidth: "100%",
    position: "relative",
    zIndex: 1,
    "@media screen and (max-width:720px)": {
      flexBasis: "100%",
    },
  },
}));
