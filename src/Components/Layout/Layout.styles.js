import { createUseStyles } from "react-jss";

export default createUseStyles({
  root: {
    backgroundColor: "#000",
    minHeight: "100vh",
    width: "100%",
    padding: 20,
    overflow: "hidden",
  },
  contentContainer: {
    position: "relative",
  },
});
