import React from "react";
import PropTypes from "prop-types";
import Styles from "./Layout.styles";
import Header from "../Header/Header";

const Layout = ({ children }) => {
  const classes = Styles();

  return (
    <div className={classes.root}>
      <Header />
      <div className={classes.contentContainer}>{children}</div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
