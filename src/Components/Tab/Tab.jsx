import React from "react";
import PropTypes from "prop-types";
import Styles from "./Tab.styles";

const Tab = ({ children, onClick, isActive }) => {
  const classes = Styles();
  return (
    <h3
      onClick={onClick}
      className={`${classes.root} ${isActive ? classes.activeTab : ""}`}
    >
      {children}
    </h3>
  );
};

Tab.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
};

export default React.memo(Tab);
