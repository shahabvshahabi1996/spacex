import { createUseStyles } from "react-jss";

export default createUseStyles(({ superColor, textColor }) => ({
  root: {
    marginRight: 20,
    verticalAlign: "middle",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    transition: "color 0.5s",
    color: textColor,
    "&:hover": {
      color: superColor,
    },
  },

  activeTab: {
    color: superColor,
  },
}));
