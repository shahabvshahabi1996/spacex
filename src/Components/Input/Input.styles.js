import { createUseStyles } from "react-jss";

export default createUseStyles(({ textColor, borderStyle }) => ({
  root: {
    border: borderStyle,
    padding: 10,
    backgroundColor: "transparent",
    width: "100%",
    fontSize: 20,
    color: textColor,
    fontFamily: "SpaceX , sans-serif",
    fontWeight: 500,
    "&:focus": {
      outline: "none",
    },
  },
}));
