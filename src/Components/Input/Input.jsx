import React from "react";
import PropTypes from "prop-types";
import Styles from "./Input.styles";

const Input = ({ className, onChange, placeholder, ...rest }) => {
  const classes = Styles();
  return (
    <input
      className={`${classes.root} ${className}`}
      placeholder={placeholder}
      onChange={onChange}
      {...rest}
    />
  );
};

Input.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default Input;
