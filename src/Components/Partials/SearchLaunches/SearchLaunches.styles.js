import { createUseStyles } from "react-jss";

export default createUseStyles({
  input: {
    marginBottom: 30,
  },
});
