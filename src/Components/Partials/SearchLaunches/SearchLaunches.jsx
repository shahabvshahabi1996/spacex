import React, { useState, useEffect } from "react";
import Styles from "./SearchLaunches.styles";
import { LaunchCard } from "../../Card/Card";
import LazyLoad from "react-lazyload";
import useDebounce from "../../Hooks/useDebounce";
import API from "../../../Constants/api";
import { queryMaker } from "../../../Constants/helpers";
import { SEARCH_LAUNCHES } from "../../../Constants/constants";
import Input from "../../Input/Input";
const api = new API();

const SearchLaunches = () => {
  const classes = Styles();
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [data, setData] = useState([]);
  const [value, setValue] = useState("");
  const debouncedValue = useDebounce(value);

  useEffect(() => {
    if (debouncedValue.length > 0) {
      fetchSearchQuery();
    }
  }, [debouncedValue]);

  const fetchSearchQuery = () => {
    setLoading(true);

    const data = { site_name: debouncedValue };
    let queries = queryMaker(data);

    api
      ._GET(SEARCH_LAUNCHES(queries))
      .then((res) => {
        setLoading(false);
        setData(res.data);
      })
      .catch(() => {
        setLoading(false);
        setIsError(true);
      });
  };

  const handleChangeInput = (e) => {
    setValue(e.target.value);
    return;
  };

  const renderResult = () => {
    if (isError) {
      return <div>Error on fetch Data</div>;
    } else if (data.length === 0 && debouncedValue.length === 0) {
      return <div>Search by site name</div>;
    }

    return (
      <div className={classes.cardWrapper}>
        {data.length > 0 ? (
          data.map((item, index) => (
            <LazyLoad key={item.flight_id} offset={100} height={100}>
              <LaunchCard
                index={index}
                length={data.length}
                title={item.mission_name}
                date={item.mission_date}
                description={item.details}
                siteName={item.launch_site.site_name}
              />
            </LazyLoad>
          ))
        ) : (
          <div>NO DATA FOUND</div>
        )}
      </div>
    );
  };

  return (
    <div>
      <Input
        onChange={handleChangeInput}
        className={classes.input}
        placeholder="Search by site name"
      />
      <div className={classes.resultWrapper}>
        {loading ? <div>Loading ...</div> : renderResult()}
      </div>
    </div>
  );
};

export default SearchLaunches;
