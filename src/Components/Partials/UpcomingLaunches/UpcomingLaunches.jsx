import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { LaunchCard } from "../../Card/Card";
import { fetchUpcomingLaunches } from "../../../Redux/actions/fetchActions";
import LazyLoad from "react-lazyload";

const UpcomingLaunches = () => {
  const { data, loading, isError } = useSelector(
    (state) => state.launches.upcoming
  );
  const dispatch = useDispatch();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    if (data.length === 0) {
      dispatch(fetchUpcomingLaunches());
      return;
    }
    return;
  };

  const renderCards = () => {
    if (isError) {
      return <h5>Error on fetching data!</h5>;
    }

    return (
      <div>
        {data.map((item, index) => (
          <LazyLoad key={item.flight_number} offset={100} height={100}>
            <LaunchCard
              title={item.mission_name}
              description={item.details}
              date={item.launch_date_utc}
              index={index}
              length={data.length}
              siteName={item.launch_site.site_name}
            />
          </LazyLoad>
        ))}
      </div>
    );
  };

  return <div>{loading ? <h5>Loading....</h5> : renderCards()}</div>;
};

export default UpcomingLaunches;
