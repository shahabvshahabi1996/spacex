import { createUseStyles } from "react-jss";

export default createUseStyles({
  cardWrapper: {
    display: "flex",
    alignItems: "flex-start",
    flexWrap: "wrap",
  },
});
