import React, { useEffect } from "react";
import { fetchHomeMissions } from "../../../Redux/actions/fetchActions";
import { useDispatch, useSelector } from "react-redux";
import Card from "../../Card/Card";
import Styles from "./HomeMission.styles";

const HomeMission = () => {
  const { data, loading, isError } = useSelector(
    (state) => state.missions.home
  );
  const dispatch = useDispatch();
  const classes = Styles();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    if (data.length === 0) {
      dispatch(fetchHomeMissions());
      return;
    }

    return;
  };

  const renderHomeMissions = () => {
    if (isError) {
      return <h5>Error on fetching data!</h5>;
    }

    return data.length > 0 ? (
      data.map((item) => (
        <Card
          key={item.mission_id}
          title={item.mission_name}
          description={item.description}
          link={item.wikipedia}
        />
      ))
    ) : (
      <h5>NO DATA</h5>
    );
  };

  return (
    <div className={classes.cardWrapper}>
      {loading ? <h5>Loading....</h5> : renderHomeMissions()}
    </div>
  );
};

export default HomeMission;
