import React, { useEffect } from "react";
import { fetchAllMissions } from "../../../Redux/actions/fetchActions";
import { useDispatch, useSelector } from "react-redux";
import { MissionCard } from "../../Card/Card";
import LazyLoad from "react-lazyload";

const AllMissions = () => {
  const dispatch = useDispatch();
  const { data, loading, isError } = useSelector((state) => state.missions.all);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    if (data.length === 0) {
      dispatch(fetchAllMissions());
      return;
    }
    return;
  };

  const renderAllMissions = () => {
    if (isError) {
      return <h5>Error on fetching data!</h5>;
    }

    return data.length > 0 ? (
      data.map((item, index) => (
        <LazyLoad key={item.mission_id} offset={100} height={100}>
          <MissionCard
            withAnimation={true}
            title={item.mission_name}
            description={item.description}
            link={item.wikipedia}
            index={index}
            length={data.length}
          />
        </LazyLoad>
      ))
    ) : (
      <div>NO DATA</div>
    );
  };

  return <div>{loading ? <h5>Loading ...</h5> : renderAllMissions()}</div>;
};

export default AllMissions;
