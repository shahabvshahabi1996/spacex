import React from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import Styles from "./LinkItem.styles";
import PropTypes from "prop-types";

const LinkItem = ({ path, children }) => {
  const pathName = useHistory().location.pathname;
  const classes = Styles();
  return (
    <Link className={pathName === path ? classes.activeLink : ""} to={path}>
      {children}
    </Link>
  );
};

LinkItem.propTypes = {
  path: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default LinkItem;
