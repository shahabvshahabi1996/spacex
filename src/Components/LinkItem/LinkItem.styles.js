import { createUseStyles } from "react-jss";

export default createUseStyles(({ superColor }) => ({
  activeLink: {
    color: `${superColor} !important`,
  },
}));
