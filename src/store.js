import { createStore, compose, applyMiddleware } from "redux";
import rootReducer from "./Redux/reducers";
import thunk from "redux-thunk";

const initalState = {};
const middlewares = [thunk];

const composeEnhancers =
  (process.env.NODE_ENV === "development" &&
    typeof window !== "undefined" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const store = createStore(
  rootReducer,
  initalState,
  composeEnhancers(applyMiddleware(...middlewares))
);

export default store;
